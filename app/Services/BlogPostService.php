<?php

namespace App\Services;

use App\Models\BlogPostModelInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use function PHPUnit\Framework\isEmpty;

/**
 * Class BlogPostService
 *
 * @package App\Services
 */
class BlogPostService implements BlogPostServiceInterface
{
    public const POST_TITLE      = 'title';
    public const POST_TEXT       = 'text';
    public const POST_CREATED_AT = 'created_at';
    public const POST_UPDATED_AT = 'updated_at';
    public const POST_IMAGE_URL  = 'image_url';
    public const POST_USER_ID    = 'user_id';
    public const POST_ID         = 'id';


    function __construct(
        private BlogPostModelInterface $blogPostModel,
        private int $item_per_page,
        private string $imagesDirectory
    )
    {
    }


    /**
     * Add post
     *
     * @param array   $params
     * @param Request $request
     *
     * @return bool
     * @throws Exception
     */
    public function addBlogPost(array $params, Request $request): bool
    {
        if (!isset($params[self::POST_TITLE]) || !isset($params[self::POST_TEXT])) {
            throw new Exception('Missing parameters');
        }

        if (empty($params[self::POST_TITLE]) || empty($params[self::POST_TEXT])) {
            throw new Exception('Parameters cannot be empty!');
        }

        if ($request->files->get(self::POST_IMAGE_URL)) {

            if (!in_array($request->files->get(self::POST_IMAGE_URL)->guessExtension(), ["jpg", "png", "jpeg"])) {

                throw new Exception('Only JPEG or png image allowed');
            }

            $fileName                     = uniqid() . "." . $request->files->get(self::POST_IMAGE_URL)->guessExtension();
            $image_url                    = $this->imagesDirectory . $fileName;
            $params[self::POST_IMAGE_URL] = $image_url;
            $request->files->get('image_url')
                ->move(__DIR__ . "/../.." . $this->imagesDirectory, $fileName);
        }

        return $this->blogPostModel->addBlogPost($params);
    }


    /**
     * Edit post
     *
     * @param array $params
     *
     * @return bool
     * @throws Exception
     */
    public function editBlogPost(array $params): bool
    {
        if (!isset($params[self::POST_TITLE]) || !isset($params[self::POST_TEXT]) || !isset($params[self::POST_ID])) {
            throw new Exception('Missing parameters');
        }

        if (empty($params[self::POST_TITLE]) || empty($params[self::POST_TEXT]) || $params[self::POST_ID] === 0) {
            throw new Exception('Empty parameters value!');
        }

        return $this->blogPostModel->updateBlogPost($params);
    }


    /**
     * List blog post
     *
     * @return array
     * @throws Exception
     */
    public function listPostsByUserid(): array
    {
        return $this->blogPostModel->blogPostListByUserid();
    }


    /**
     * List blog post
     *
     * @param int $pageNumber
     *
     * @return array
     */
    public function listPosts(int $pageNumber): array
    {
        return $this->blogPostModel->blogPostList($pageNumber, $this->item_per_page);
    }


    /**
     * Get blog post by id
     *
     * @param int $id
     *
     * @return array
     * @throws Exception
     */
    public function getBlogPostById(int $id): array
    {
        return $this->blogPostModel->getBlogPostById($id);
    }
}