<?php

namespace App\Models;

use App\Services\BlogPostService;
use App\Services\DatabaseConnectionServiceInterface;
use Exception;
use PDO;

/**
 * Class BlogPostModel
 *
 * @package App\Models
 */
class BlogPostModel implements BlogPostModelInterface
{
    function __construct(
        private DatabaseConnectionServiceInterface $connection
    )
    {
    }


    /**
     * Add blog post
     *
     * @param array $params
     *
     * @return array
     * @throws Exception
     */
    public function addBlogPost(array $params): bool
    {
        $pdo = $this->connection->getConnection();
        try {
            $data = [
                BlogPostService::POST_TITLE      => $params[BlogPostService::POST_TITLE],
                BlogPostService::POST_TEXT       => $params[BlogPostService::POST_TEXT],
                BlogPostService::POST_CREATED_AT => date('Y-m-d H:i:s'),
                BlogPostService::POST_UPDATED_AT => date('Y-m-d H:i:s'),
                BlogPostService::POST_IMAGE_URL  => $params[BlogPostService::POST_IMAGE_URL] ?? null,
                BlogPostService::POST_USER_ID    => $_SESSION['user']['id'],
            ];
            $sql  =
                "INSERT INTO blog_posts (title, text, created_at, updated_at, image_url, user_id) VALUES (:title, :text, :created_at, :updated_at, :image_url, :user_id)";
            $stmt = $pdo->prepare($sql);
            $stmt->execute($data);
        }
        catch (Exception $ex) {
            //Log the exception message here
            return false;
        }

        return true;
    }


    /**
     * Update blog post
     *
     * @param array $params
     *
     * @return array
     * @throws Exception
     */
    public function updateBlogPost(array $params): bool
    {
        $pdo = $this->connection->getConnection();
        try {
            $data = [
                BlogPostService::POST_TITLE      => $params[BlogPostService::POST_TITLE],
                BlogPostService::POST_TEXT       => $params[BlogPostService::POST_TEXT],
                BlogPostService::POST_UPDATED_AT => date('Y-m-d H:i:s'),
                BlogPostService::POST_IMAGE_URL  => $params[BlogPostService::POST_IMAGE_URL] ?? null,
                BlogPostService::POST_ID         => $params[BlogPostService::POST_ID],
            ];

            $sql  =
                "UPDATE blog_posts SET title=:title, text=:text, updated_at=:updated_at, image_url=:image_url WHERE id=:id";
            $stmt = $pdo->prepare($sql);
            $stmt->execute($data);
        }
        catch (Exception $ex) {
            //Log the exception message here
            return false;
        }

        return true;
    }


    /**
     * List blog post by user id
     *
     * @return bool
     * @throws Exception
     */
    public function blogPostListByUserid(): array
    {
        $pdo = $this->connection->getConnection();
        try {
            $sql       = 'SELECT * FROM blog_posts WHERE user_id = :user_id';
            $statement = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $statement->execute(
                [
                    ':user_id' => $_SESSION['user']['id'],
                ]
            );
            $result = $statement->fetchAll();
        }
        catch (Exception $ex) {
            //Log the exception message here
            return [];
        }

        return $result;
    }


    /**
     * Get blog post by id
     *
     * @return bool
     * @throws Exception
     */
    public function getBlogPostById(int $id): array
    {
        $pdo = $this->connection->getConnection();
        try {
            $sql       =
                'SELECT blog_posts.*, users.first_name, users.last_name FROM blog_posts INNER JOIN users ON blog_posts.user_id=users.id WHERE blog_posts.id = :id';
            $statement = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $statement->execute(
                [
                    ':id' => $id,
                ]
            );
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);

            if (empty($result)) {
                return [];
            }
        }
        catch (Exception $ex) {
            //Log the exception message here
            return [];
        }

        return current($result);
    }


    /**
     * List blog posts
     *
     * @return bool
     * @throws Exception
     */
    public function blogPostList(int $page, int $items_per_page): array
    {

        $offset = ($page - 1) * $items_per_page;
        $pdo    = $this->connection->getConnection();
        try {
            $sql       =
                'SELECT * FROM blog_posts order by updated_at DESC LIMIT :offset, :item_per_page ';
            $statement = $pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
            $statement->bindValue(':item_per_page', $items_per_page, PDO::PARAM_INT);
            $statement->execute();
            $result = $statement->fetchAll();
        }
        catch (Exception $ex) {
            //Log the exception message here
            return [];
        }

        return $result;
    }
}